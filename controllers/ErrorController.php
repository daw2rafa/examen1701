<?php
/**
*
*/
class ErrorController
{

    function __construct()
    {
        # code...
    }

    public function index($e)
    {
        // var_dump($e);
        if ($e->getCode() == 404) {
            header("HTTP/1.0 404 Not Found");
        } else {
            header("HTTP/1.0 500 Not Found");
        }
        require 'views/error/error.php';
    }


}
